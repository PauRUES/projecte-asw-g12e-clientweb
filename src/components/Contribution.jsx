import React from "react";
import JSONPretty from 'react-json-pretty';

export class Contribution extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contribution: {
                id: 1,
                title: "",
                url: "",
                content: "",
                votes: "",
                created_at: "",
                user: {
                    id: 1,
                    name: "",
                    email: "",
                    about: ""
                },
                comments: []
            }
        };
    }

    async componentDidMount(){
        const id = window.location.pathname.replace('/contribution/','');
        const url = `https://hacker-news-asw-api.herokuapp.com/contributions/${id}.json`;
        const response = await fetch(url);
        const data = await response.json();
        this.setState({contribution: data});
        console.log(data);
    }

    render(){
        return(
            <JSONPretty id="json-pretty" data={this.state.contribution}></JSONPretty>
        )
    }
}