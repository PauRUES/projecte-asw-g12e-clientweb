import React from "react";
import JSONPretty from 'react-json-pretty';

export class ContributionsUpvoted extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contributions: []
        };
    }

    async componentDidMount(){
        const requestOptions = {
            headers: { 'Content-Type': 'application/json', 'X-API-Key': 'a540ebddbb76', 'Accept': 'application/json' },
        };
        const url = `https://hacker-news-asw-api.herokuapp.com/contributions_user.json?upvoted=0`;
        const response = await fetch(url, requestOptions);
        const data = await response.json();
        this.setState({contributions: data});
        console.log(data);
    }

    render(){
        return(
            <JSONPretty id="json-pretty" data={this.state.contributions}></JSONPretty>
        )
    }
}