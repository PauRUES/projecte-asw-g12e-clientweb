import React from "react";
import JSONPretty from 'react-json-pretty';

export class Newest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contributions: []
        };
    }

    async componentDidMount(){
        const url = `https://hacker-news-asw-api.herokuapp.com/newest.json`;
        const response = await fetch(url);
        const data = await response.json();
        this.setState({contributions: data});
        console.log(data);
    }

    render(){
        return(
            <JSONPretty id="json-pretty" data={this.state.contributions}></JSONPretty>
        )
    }
}