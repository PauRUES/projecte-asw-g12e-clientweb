import React from "react";
import JSONPretty from 'react-json-pretty';

export class ContributionsUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contributions: []
        };
    }

    async componentDidMount(){
        const id = window.location.pathname.replace('/contributions_user/','');
        const url = `https://hacker-news-asw-api.herokuapp.com/contributions_user.json?user_id=${id}`;
        const response = await fetch(url);
        const data = await response.json();
        this.setState({contributions: data});
        console.log(data);
    }

    render(){
        return(
            <JSONPretty id="json-pretty" data={this.state.contributions}></JSONPretty>
        )
    }
}