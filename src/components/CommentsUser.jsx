import React from "react";
import JSONPretty from 'react-json-pretty';

export class CommentsUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: []
        };
    }

    async componentDidMount(){
        const id = window.location.pathname.replace('/comments_user/','');
        const url = `https://hacker-news-asw-api.herokuapp.com/comments_user.json?user_id=${id}`;
        const response = await fetch(url);
        const data = await response.json();
        this.setState({comments: data});
        console.log(data);
    }

    render(){
        return(
            <JSONPretty id="json-pretty" data={this.state.comments}></JSONPretty>
        )
    }
}