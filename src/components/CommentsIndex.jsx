import React from "react";
import JSONPretty from 'react-json-pretty';

export class CommentsIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: []
        };
    }

    async componentDidMount(){
        const url = `https://hacker-news-asw-api.herokuapp.com/comments.json`;
        const response = await fetch(url);
        const data = await response.json();
        this.setState({comments: data});
        console.log(data);
    }

    render(){
        console.log(this.state.contribution);
        return(
            <JSONPretty id="json-pretty" data={this.state.comments}></JSONPretty>
        )
    }
}