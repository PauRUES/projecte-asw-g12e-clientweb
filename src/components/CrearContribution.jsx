import React from 'react'


export class CrearContribution extends React.Component {

    
    constructor(props) {
        super(props); 

        this.state = {
            title: '',
            url: '',
            ask: '',
            errorMessage: ''
        }
    }

    submitHandler = e => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'X-API-Key': 'a540ebddbb76' },
            body: JSON.stringify({ title: this.state.title, url: this.state.url, ask: this.state.ask })
        };
        fetch('https://hacker-news-asw-api.herokuapp.com/contributions.json', requestOptions)
            .then(async response => {
                const isJson = response.headers.get('content-type').includes('application/json');
                const data = isJson && await response.json();
    
                // check for error response
                if (!response.ok) {
                    // get error message from body or default to response status
                    const error = (data && data.message) || response.status;
                    return Promise.reject(error);
                }
    
                //this.props.history.push('/newest');
    
    
            })
            .catch(error => {
                this.setState({ errorMessage: error.toString() });
            });

            e.preventDefault();
      }
    

    changeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }
    
    render(){
        const {title,url,ask, errorMessage} = this.state
        return (
            <div>
                <center>
                <form onSubmit={this.submitHandler}>
                    <tbody>
                        <tr>
                            <td>Title</td>
                            <td>
                                <input type="text" name="title" value={title} onChange= {this.changeHandler} />
                               
                            </td>
                        </tr>

                        <tr>
                            <td>Url</td>
                            <input type="text" name="url" value={url} onChange= {this.changeHandler}/>
                        </tr>

                        <tr>
                            <td></td>
                            <td><b>or</b></td>
                        </tr>

                        <tr>
                            <td>Text</td>
                            <td>
                                
                                <textarea type="text" name="ask" value={ask} onChange= {this.changeHandler} />
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <center><button type="submit">Submit</button></center>
                        </tr>

                        <tr>
                            <td></td>
                            <td>{this.state.errorMessage}</td>
                        </tr>

                    </tbody>
                </form>
                </center>
            </div>
        )
    }
    
}
