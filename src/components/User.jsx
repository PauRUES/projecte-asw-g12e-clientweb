import React from "react";
import JSONPretty from 'react-json-pretty';

export class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                id: 1,
                name: "",
                email: "",
                about: "",
                created_at: "",
                updated_at: "",
                karma: ""
            }
        };
    }

    async componentDidMount(){
        const id = window.location.pathname.replace('/users/','');
        const url = `https://hacker-news-asw-api.herokuapp.com/users/${id}.json`;
        const response = await fetch(url);
        const data = await response.json();
        this.setState({user: data});
        console.log(data);
    }

    render(){
        return(
            <JSONPretty id="json-pretty" data={this.state.user}></JSONPretty>
        )
    }
}