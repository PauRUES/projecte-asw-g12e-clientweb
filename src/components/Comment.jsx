import React from "react";
import JSONPretty from 'react-json-pretty';

export class Comment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: {
                id: 1,
                text: "",
                points: "",
                contribution_id: "",
                parent_id: "",
                created_at: "",
                user:{
                    id: 1,
                    name: "",
                    email: "",
                    about: ""
                }
            }
        };
    }

    async componentDidMount(){
        const id = window.location.pathname.replace('/comment/','');
        const url = `https://hacker-news-asw-api.herokuapp.com/comments/${id}.json`;
        const response = await fetch(url);
        const data = await response.json();
        this.setState({comment: data});
        console.log(data);
    }

    render(){
        return(
            <JSONPretty id="json-pretty" data={this.state.comment}></JSONPretty>
        )
    }
}