import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {TodoList} from './components/TodoList';
import {User} from './components/User.jsx'
import {Contribution} from './components/Contribution.jsx'
import {ContributionsIndex} from './components/ContributionsIndex.jsx'
import {Ask} from './components/Ask.jsx'
import {Newest} from './components/Newest.jsx'
import {ContributionsUser} from './components/ContributionsUser.jsx'
import {ContributionsUpvoted} from './components/ContributionsUpvoted.jsx'
import {Comment} from './components/Comment.jsx'
import {CommentsIndex} from './components/CommentsIndex.jsx'
import {CommentsUser} from './components/CommentsUser.jsx'
import {CommentsUpvoted} from './components/CommentsUpvoted.jsx'
import {CrearContribution} from './components/CrearContribution.jsx'

export class App extends React.Component {

      render() {
        return(
            <BrowserRouter>
                <Switch>
                    <Route path='/submit' component={CrearContribution}/>
                    <Route path='/users' component={User} />
                    <Route path='/contribution' component={Contribution} />
                    <Route path='/contributions' component={ContributionsIndex} />
                    <Route path='/ask' component={Ask} />
                    <Route path='/newest' component={Newest} />
                    <Route path='/contributions_user' component={ContributionsUser} />
                    <Route path='/contributions_upvoted' component={ContributionsUpvoted} />
                    <Route path='/comment' component={Comment} />
                    <Route path='/comments' component={CommentsIndex} />
                    <Route path='/comments_user' component={CommentsUser} />
                    <Route path='/comments_upvoted' component={CommentsUpvoted} />
                    <Route path='/' component={ContributionsIndex} />
                </Switch>
            </BrowserRouter>
        )
      }
}